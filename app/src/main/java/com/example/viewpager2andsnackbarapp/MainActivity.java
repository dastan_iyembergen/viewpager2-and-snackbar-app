package com.example.viewpager2andsnackbarapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    ViewPager2 viewPager;
    Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyPagerAdapter adapter = new MyPagerAdapter(this);
        viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(adapter);

        snackbar = Snackbar.make(viewPager,
                "Page: ",
                Snackbar.LENGTH_SHORT);
        snackbar.setAction("OK", new View.OnClickListener(){
            @Override
            public void onClick(View view){
                snackbar.dismiss();
                Log.e("TAG", "Snackbar Action");
            }
        });

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                snackbar.setText("Page: " + position);
                snackbar.show();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }
}
